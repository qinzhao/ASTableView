#ASTableView
> 
1、UITableView的流畅性优化，fps保持在57-60左右.非常的流畅。
> 
2、加上了动态获取图片。按照图片的比例大小显示。分为俩个页面，第一个是自己根据图片来自适应图片大小，第二个根据json来适应图片大小。我更青睐于后者。
> 
3、该代码感谢YYKit,Masonry,以及UITableView+FDTemplateLayoutCell的作者.
> 
4、讲YYkit的fps指示器，封装了下 可直接 pod  "ASFPSManager"
>

该代码会继续更新，如果有问题issues.谢谢！